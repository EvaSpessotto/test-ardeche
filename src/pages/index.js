import React from "react"
import "bootstrap/dist/css/bootstrap.min.css"
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from "reactstrap"
import { graphql } from "gatsby"

class Index extends React.Component {
  render() {
    const { edges: posts } = this.props.data.allMarkdownRemark
    return (
      <Container>
        <Row className="text-center">
          <Col>
            <h1>Blog</h1>
          </Col>
        </Row>
        <Row>
          {posts
            .filter(post => post.node.frontmatter.title.length > 0)
            .map(({ node: { frontmatter: post } }) => {
              return (
                <Col lg="4">
                  <Card>
                    <CardBody>
                      <CardTitle>{post.title}</CardTitle>
                      <CardSubtitle>Card subtitle</CardSubtitle>
                      <CardText>
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </CardText>
                      <Button>Button</Button>
                    </CardBody>
                  </Card>
                </Col>
              )
            })}
        </Row>
      </Container>
    )
  }
}

export default Index

export const query = graphql`
  query Index {
    allMarkdownRemark {
      edges {
        node {
          frontmatter {
            title
          }
        }
      }
    }
  }
`
